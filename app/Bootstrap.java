import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import play.*;
import play.db.jpa.Blob;
import play.jobs.*;
import play.libs.MimeTypes;
import play.test.*;
import models.*;

@OnApplicationStart
public class Bootstrap extends Job {
	public void doJob() throws FileNotFoundException {
		
		//Preload yaml file
		Fixtures.deleteDatabase();
		Fixtures.loadModels("data.yml");

		//preload profile images and profile thumbnails.
		User user = null;
		for (long i = 1; i <= 9; i++)
		{
			//find the user
			user = user.findById(i);
			//create string for profile photo file path
			String userPhoto = "public/images/" + user.lastName + ".png";
			//Logger.info("File path for profile photo: " + userPhoto);
			//create string for thumbnail file path			
			String userThumbnail = "public/images/" + user.lastName + "-thumb.png";
			//Logger.info("File path for thumbnail: " + userThumbnail);
			
			//create blob for profile photo
			Blob profilePhoto = new Blob();
			profilePhoto.set(new FileInputStream(userPhoto),
					MimeTypes.getContentType(userPhoto));
			
			//create blob for thumbnail
			Blob thumbnail = new Blob();
			thumbnail.set(new FileInputStream(userThumbnail),
					MimeTypes.getContentType(userThumbnail));
			
			//add blobs to user and save user	
			user.profilePicture = profilePhoto;
			user.thumbnailPicture = thumbnail;
			user.save();
		}
		
		//preload image to post nibblerp1
		String image = "public/images/brains.jpg";
		Blob postImage = new Blob();
		postImage.set(new FileInputStream(image),
		    MimeTypes.getContentType(image));
		
		Post post = Post.findById(Long.parseLong("1"));
		post.image = postImage;
		post.containsImage = true;
		post.save();
		
	}
}