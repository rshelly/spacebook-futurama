package models;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import java.util.ArrayList;
import java.util.List;

import play.Logger;
import play.db.jpa.Model;
import play.db.jpa.Blob;

@Entity
public class User extends Model
{
  public String firstName;
  public String lastName;
  public String email;
  public String password;
  public String statusText;
  public Blob   profilePicture;
  public Blob	thumbnailPicture;
  public int    age;
  public String species;
  public boolean online;
  public boolean blog;

  @OneToMany(mappedBy = "sourceUser")
  public List<Friendship> friendships = new ArrayList<Friendship>();
  
  @OneToMany (mappedBy = "targetUser")
  public List<Friendship> friendshipsReceived = new ArrayList<Friendship>();
  
  @OneToMany(mappedBy = "to")
  public List<Message> inbox = new ArrayList<Message>();
  
  @OneToMany(mappedBy = "from")
  public List<Message> outbox = new ArrayList<Message>();
  
  @OneToMany (mappedBy = "blogger")
  public List<Post> posts = new ArrayList<Post>();
  
  @OneToMany (mappedBy = "sender")
  public List<Comment> commentsMade = new ArrayList<Comment>();
  
  public User(String firstName, String lastName, String email, String password, int age, String species)
  {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.password = password;
    this.age = age;
    this.species = species;
    blog = false;
  }
  
  public static User findByEmail(String email)
  {
    return find("email", email).first();
  }

  public boolean checkPassword(String password)
  {
    return this.password.equals(password);
  }  
  
  public void befriend(User friend)
  {
    Friendship friendship = new Friendship(this, friend);
    friendships.add(friendship);
    friendship.save();
    save();
  }

  public void unfriend(User friend)
  {
    Friendship thisFriendship = null;
    
    for (Friendship friendship:friendships)
    {
      if (friendship.targetUser== friend)
      {
        thisFriendship = friendship;
      }
    }
    friendships.remove(thisFriendship);
    thisFriendship.delete();
    save();
  }  
  
  public void sendMessage (User to, String messageText)
  {
    Message message = new Message (this, to, messageText);
    outbox.add(message);
    to.inbox.add(message);
    message.save();
  }
  
  public void loggedIn(boolean online)
  {
	this.online = online;
	Logger.info("User: " + firstName + ". Online :" + this.online);
  }
  
  public void setBlogger (boolean blog)
  {
	  this.blog = blog;
  }
}