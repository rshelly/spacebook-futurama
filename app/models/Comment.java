package models;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import play.db.jpa.Model;

@Entity
public class Comment extends Model {

  @ManyToOne
  public Post originalPost;
	@ManyToOne
	public User sender;
	public String commentDate;
	public String content;
	
	public Comment (User sender, String content, Post post){
		
		this.sender = sender;
		this.content = content;
				
		Date date = new Date();
		SimpleDateFormat  dateFormat =
				new SimpleDateFormat ("E',' MMM dd 'at' hh.mm aa");
		
		commentDate = dateFormat.format(date);
		
		originalPost = post;
	}
}
