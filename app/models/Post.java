package models;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import play.Logger;
import play.db.jpa.Blob;
import play.db.jpa.Model;

@Entity
public class Post extends Model
{
  public String title;
  @Lob
  public String content;
  
  @ManyToOne
  public User blogger;
  
  public Blob image;
  public Boolean containsImage;
  
  @OneToMany (mappedBy = "originalPost")
  public List<Comment> comments = new ArrayList<Comment>();


  public Post(String title, String content, User blogger)
  {
    this.title = title;
    this.content = content;
    this.blogger = blogger;
    image = null;
    containsImage = false;
  }

  public void addComment (User sender, String content, Post post)
  {
	  Comment comment =  new Comment(sender, content, post);
	  comments.add(comment);
	  comment.save();
  }
  
//  public void setContainsImage(Boolean containsImage)
//  {
//    this.containsImage = containsImage;
//  }
  
  public String toString()
  {
    return title;
  } 
}