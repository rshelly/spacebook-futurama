package controllers;

import play.*;
import play.db.jpa.Blob;
import play.mvc.*;

import java.util.*;

import org.hibernate.connection.UserSuppliedConnectionProvider;

import models.*;

public class EditProfile extends Controller {
  public static void change(String firstName, String lastName, int age,
      String species, String email, String password) {
    User user = Accounts.getLoggedInUser();
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.species = species;
    user.age = age;
    user.password = password;
    user.save();
    Profile.index();
  }

  public static void index() {
    User user = Accounts.getLoggedInUser();
    render(user);
  }

  public static void uploadPicture(Long id, Blob picture) {
    if (picture != null) {
      User user = User.findById(id);
      user.profilePicture = picture;
      user.save();
      Logger.info("saving picture");
      Home.index();
    } else {
      Home.index();
    }
  }

  public static void uploadThumbnail(Long id, Blob picture) {
    if (picture != null) {
      User user = User.findById(id);
      user.thumbnailPicture = picture;
      user.save();
      index();
    } else {
      index();
    }
  }

  public static void deleteAccount(Long id, String password) {

    User user = User.findById(id);
    if (password.equals(user.password)) {

      // delete users blog (all posts + associated comments)
      if (user.blog) {
        // run through all posts
        for (int i = user.posts.size(); i > 0; i--) {

          Post postToDelete = user.posts.get(i - 1);
          // delete any comments on the post
          for (int j = postToDelete.comments.size(); j > 0; j--) {
            Comment commentToDelete = postToDelete.comments.get(j - 1);
            postToDelete.comments.remove(commentToDelete);
            commentToDelete.delete();
          }
          user.posts.remove(postToDelete);
          postToDelete.delete();
        }
      }
      
      //delete comments by user made on other user blogs
      if (user.commentsMade.size() > 0) {
        for (int i = user.commentsMade.size(); i > 0; i--) {
          Comment commentMadeToDelete = user.commentsMade.get(i -1);
          Post sourcePost = commentMadeToDelete.originalPost;
          sourcePost.comments.remove(commentMadeToDelete);
          sourcePost.save();
          commentMadeToDelete.delete();
        }
      }

      // delete users messages (both sent and received
      // delete all messages from users inbox and from senders outbox
      if (user.inbox.size() > 0) {
        for (int i = user.inbox.size(); i > 0; i--) {
          Message receivedMessageToDelete = user.inbox.get(i - 1);
          receivedMessageToDelete.from.outbox.remove(receivedMessageToDelete);
          receivedMessageToDelete.from.save();
          user.inbox.remove(receivedMessageToDelete);
          receivedMessageToDelete.delete();
        }
      }
      // delete all messages from users outbox and from recipients inbox
      if (user.outbox.size() > 0) {
        for (int i = user.outbox.size(); i > 0; i--) {
          Message sentMessageToDelete = user.outbox.get(i - 1);
          sentMessageToDelete.to.inbox.remove(sentMessageToDelete);
          sentMessageToDelete.to.save();
          user.outbox.remove(sentMessageToDelete);
          sentMessageToDelete.delete();
        }
      }

      // delete all necessary friendships
      // delete all users friends
      if (user.friendships.size() > 0) {
        for (int i = user.friendships.size(); i > 0; i--) {
          Friendship friendshipToDelete = user.friendships.get(i - 1);
          user.friendships.remove(friendshipToDelete);
          friendshipToDelete.delete();
          friendshipToDelete.targetUser.save();
        }
      }
      
      //delete user from other users friend lists
      if (user.friendshipsReceived.size() > 0) {
        for (int i = user.friendshipsReceived.size(); i > 0; i--) {
          Friendship friendshipReceivedToDelete = user.friendshipsReceived.get(i - 1);
          User sourceUser = friendshipReceivedToDelete.sourceUser;
          user.friendshipsReceived.remove(friendshipReceivedToDelete);
          sourceUser.friendships.remove(friendshipReceivedToDelete);
          friendshipReceivedToDelete.delete();
          sourceUser.save();
        }
      }

      // delete user
      user.save();
      user.delete();

      // go to landing page
      Accounts.index();
      

    }
  }
}
