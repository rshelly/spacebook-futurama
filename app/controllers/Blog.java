package controllers;

import java.util.ArrayList;
import java.util.List;

import models.Comment;
import models.Message;
import models.Post;
import models.User;
import play.Logger;
import play.mvc.Controller;

public class Blog extends Controller {

  public static void index() {
    User user = Accounts.getLoggedInUser();
    render(user);
  }

  public static void newPost(String title, String content) {
    if ((content.length() > 0) && (title.length() > 0)) {
      User user = Accounts.getLoggedInUser();

      Post post = new Post(title, content, user);
      post.save();
      user.posts.add(0, post);
      user.setBlogger(true);
      user.save();

      Logger.info("title:" + title);
      index();
    } else {
      index();
    }
  }

  public static void deletePost(Long postid) {
    User user = Accounts.getLoggedInUser();
    Post post = Post.findById(postid);

    for (int i = post.comments.size(); i > 0; i--) {
      Comment comment = post.comments.get(i - 1);
      post.comments.remove(i - 1);
      comment.delete();
    }

    user.posts.remove(post);

    if (user.posts.size() == 0) {
      user.setBlogger(false);
    }

    user.save();
    post.delete();

    index();
  }
}