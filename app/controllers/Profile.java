package controllers;

import play.*;
import play.db.jpa.Blob;
import play.mvc.*;

import java.util.*;

import models.*;

public class Profile extends Controller {
	
	public static void index() {
		User user = Accounts.getLoggedInUser();		
	    boolean showAge = true;
	    render(user, showAge);
	}

	public static void changeStatus(String statusText) {
		if (statusText.length() > 0) {
			User user = Accounts.getLoggedInUser();
			user.statusText = statusText;
			user.save();
			Logger.info("Status changed to " + statusText);
			index();
		} else {
			index();
		}
	}

	public static void getPicture(Long id) {
		User user = User.findById(id);
		Blob picture = user.profilePicture;
		if (picture.exists()) {
			response.setContentTypeIfNotSet(picture.type());
			renderBinary(picture.get());
		}
	}

	public static void getThumbnail(Long id) {
		User user = User.findById(id);
		Blob thumbnail = user.thumbnailPicture;
		if (thumbnail.exists()) {
			response.setContentTypeIfNotSet(thumbnail.type());
			renderBinary(thumbnail.get());
		}
	}
}