package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Members extends Controller {
	public static void index() {
		User user = Accounts.getLoggedInUser();
		List<User> users = User.findAll();
		users.remove(user);
		render(user, users);
	}

	public static void follow(Long id) {
		User user = Accounts.getLoggedInUser();
		User friend = User.findById(id);
		if (!checkFriends(user, friend))
		{
			user.befriend(friend);
			Home.index();
		}
		else
		{
			Home.index();
		}
	}

	/**
	 * This method checks if two users are already friends
	 * 
	 * @param user
	 *            logged in user
	 * @param friend
	 *            user targeted for friendship
	 * @return true if target is already friend of logged in user false
	 *         otherwise
	 */
	private static boolean checkFriends(User user, User friend) {
		for (Friendship friendship : user.friendships)
			if (friend == friendship.targetUser) {
				return true;
			}
		return false;
	}
}