package controllers;

import models.Post;
import models.User;
import play.Logger;
import play.db.jpa.Blob;
import play.mvc.Controller;

public class BlogEntry extends Controller {
	
	  public static void view(Long postid)
	  {
	    User user = Accounts.getLoggedInUser();
	    
	    Post post = Post.findById(postid);
	    
	    render(user, post);
	  }
	  
	  public static void getPicture(Long id) {
	    Post post = Post.findById(id);
	    Blob picture = post.image;
	    if (picture.exists()) {
	      response.setContentTypeIfNotSet(picture.type());
	      renderBinary(picture.get());
	    }
	  }
	  
	  public static void uploadPostImage(Long id, Blob picture) {
	    if (picture != null) {
	      Post post = Post.findById(id);
	      post.image = picture;
	      Logger.info("Image added to post:" + post.title);
	      post.containsImage = true;
	      Logger.info("Post: " + post.title + " contains image? " + post.containsImage);
	      post.save();
	      view(id);
	    }
	    else {
	      view(id);
	    }
	  }
}