package controllers;

import models.Comment;
import models.Post;
import models.User;
import play.Logger;
import play.mvc.Controller;

public class PublicBlog extends Controller {

  public static void index(Long bloggerId) {
    User currentUser = null;
    if (session.contains("logged_in_userid")) {
      currentUser = Accounts.getLoggedInUser();
    }

    User user = User.findById(bloggerId);
    render(currentUser, user);
  }

  public static void leaveComment(Long postId, String commentText) {

    if (commentText.length() > 0) {
      // find post concerned
      Post post = Post.findById(postId);
      Logger.info("Leaving comment on post: " + post.title);

      // find user leaving comment
      User currentUser = Accounts.getLoggedInUser();

      // create and save comment
      Comment comment = new Comment(currentUser, commentText, post);
      comment.save();

      // add comment to post and save post
      post.comments.add(comment);
      post.save();

      // find user who created blog
      User user = User.findById(post.blogger.id);

      // render the blog page with current user logged in
      index(user.id);
    } else {
      Post post = Post.findById(postId);
      User user = User.findById(post.blogger.id);
      index(user.id);
    }
  }
}
