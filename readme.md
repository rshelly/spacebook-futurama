# Spacebook

Assignment for Web Development. Spacebook is a themed social media website modelled on Facebook.

[Deployed to heroku]
(https://nameless-plains-2941.herokuapp.com/)

## Features:

* Create account
* View other members
* Befriend other members
* Send messages to friends
* Upload pictures, comments on pictures
* Maintain blogs
* Delete account

## Languages

* Java
* Javascript
* HTML

## Frameworks

* Play 1.2.7 Framework
* Semantic UI